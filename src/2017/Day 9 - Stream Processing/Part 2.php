<?php

/*
--- Part Two ---

Now, you're ready to remove the garbage.

To prove you've removed it, you need to count all of the characters within
the garbage. The leading and trailing < and > don't count, nor do any
canceled characters or the ! doing the canceling.

- <>, 0 characters.
- <random characters>, 17 characters.
- <<<<>, 3 characters.
- <{!>}>, 2 characters.
- <!!>, 0 characters.
- <!!!>>, 0 characters.
- <{o"i!a,<{i<a>, 10 characters.

How many non-canceled characters are within the garbage in your puzzle
input?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Determines how many non-canceled characters are within the
 * garbage in your puzzle input.
 *
 * @param string $blocksStream
 *
 * @return int Non-canceled characters
 */
function solution(string $blocksStream): int
{
    // Removing ignore characters
    $ignorePattern = '/!./';
    $blocksStream = preg_replace($ignorePattern, '', $blocksStream);

    $charactersCountBeforeGarbageRemoval = strlen($blocksStream);

    // Cleaning from garbage
    $garbagePattern = '/<.*?>/';
    $blocksStream = preg_replace($garbagePattern, '', $blocksStream, -1, $replacements);

    $charactersCountAfterGarbageRemoval = strlen($blocksStream);

    // < and > don't count
    $nonCanceledCharacters = $charactersCountBeforeGarbageRemoval
        - $charactersCountAfterGarbageRemoval - $replacements * 2;

    return $nonCanceledCharacters;
}

$blocksStream = file_get_contents('blocksStream.txt');

$nonCanceledCharacters = solution($blocksStream);
echo $nonCanceledCharacters;
