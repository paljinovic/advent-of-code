<?php

/*
--- Day 1: Inverse Captcha ---

The night before Christmas, one of Santa's Elves calls you in a panic. "The
printer's broken! We can't print the Naughty or Nice List!" By the time you
make it to sub-basement 17, there are only a few minutes until midnight.
"We have a big problem," she says; "there must be almost fifty bugs in this
system, but nothing else can print The List. Stand in this square, quick!
There's no time to explain; if you can convince them to pay you in stars,
you'll be able to--" She pulls a lever and the world goes blurry.

When your eyes can focus again, everything seems a lot more pixelated than
before. She must have sent you inside the computer! You check the system
clock: 25 milliseconds until midnight. With that much time, you should be
able to collect all fifty stars by December 25th.

Collect stars by solving puzzles. Two puzzles will be made available on each
millisecond in the advent calendar; the second puzzle is unlocked
when you complete the first. Each puzzle grants one star. Good luck!

You're standing in a room with "digitization quarantine" written in LEDs
along one wall. The only door is locked, but it includes a small interface.
"Restricted Area - Strictly No Digitized Users Allowed."

It goes on to explain that you may only leave by solving a captcha to prove
you're not a human. Apparently, you only get one millisecond to solve the
captcha: too fast for a normal human, but it feels like hours to you.

The captcha requires you to review a sequence of digits (your puzzle input)
and find the sum of all digits that match the next digit in the list. The
list is circular, so the digit after the last digit is the first digit in
the list.

For example:

- 1122 produces a sum of 3 (1 + 2) because the first digit (1) matches
  the second digit and the third digit (2) matches the fourth digit.
- 1111 produces 4 because each digit (all 1) matches the next.
- 1234 produces 0 because no digit matches the next.
- 91212129 produces 9 because the only digit that matches the next one
  is the last digit, 9.

What is the solution to your captcha?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Finds the sum of all digits that match the next digit in the circular list.
 *
 * @param string $digitsList Digits list
 *
 * @return int Sum
 */
function solution(string $digitsList): int
{
    $sum = 0;

    $digits = array_map('intval', str_split($digitsList, 1));

    for ($i = 0; $i < count($digits); $i++) {
        $currentDigit = $digits[$i];

        // List is circular
        if ($i + 1 < count($digits)) {
            $nextDigit = $digits[$i + 1];
        } else {
            $nextDigit = $digits[0];
        }

        // If current digit matches the next one
        if ($currentDigit === $nextDigit) {
            $sum += $currentDigit;
        }
    }

    // If last digit matches the first one
    if ($digits[count($digits) - 1] === $digits[0]) {
        $sum += $digits[count($digits) - 1];
    }

    return $sum;
}

$digitsList = '599452122679583848618887218995255147535292914535728498346367894477722813939811764912984385'
.'3837124228353689551178129353548331779783742915361343229141538334688254819714813664439268791978215553677'
.'7728388533288353454847112297674777299484733912287764864566862651148756865369264986344956956922521593739'
.'7163154359465695449411714929464887666115753485193893395478761214643657118314449467995245232598921248121'
.'9139686138139314915852774628718443532415524776642877131763359413822986619312862889689472397776968662148'
.'7531877677937626541334293495153243338777879254655415885849888271366763761288878191616724671425792619954'
.'8273187897928457324653368883522635269112216984783294351375892419423234598872674178924737918431978238775'
.'7613138742817826316376233443521857881678228694863681971445442663251423184177628977899963919997529468354'
.'9535486129666995267186491327899225845245566977151331633764632562251818332576928213316655326812882169494'
.'5127684441915424542343414183491395185455125333978553339594981511562281156599925255523494455447391235967'
.'4379862182425695187593452363724591541992766651311175217218144998691121856882973825162368564156726989939'
.'9934129635368315931969976769929426735713361645359273712298232369372937823963182378797156129563177151877'
.'5739781534663545441218319864263757752863239381396451468134416281412258879586516978812165535331923379881'
.'1796765852443424783552419541481132132344487835757888468196543736833342945718867855493422435511348343711'
.'3116243997444828323859985928647952719725775485849674339173222967529921277199644533764146655761968299456'
.'6494185649376879491198453744522728565771631797464941758652839548878994668991497273228827666535617988978'
.'3557481819454699354317555417691494844812852232551189751386484638428296871436139489616192954267794441256'
.'9297838396525192858352387361429972451893638493564546456631513141248856619194514476289649967972477811968'
.'9178717164816942789428276877627568912419181175113556769231357166363721429862536765596957569985112138187'
.'2872875774999172839521617845847358966264291175387374464425566514426499166813392768677233356646752273398'
.'541814142523651415521363267414564886379863699323887278761615927993953372779567675';

$sum = solution($digitsList);
echo $sum;
