<?php

/*
--- Part Two ---

The spinlock does not short-circuit. Instead, it gets more angry. At least,
you assume that's what happened; it's spinning significantly faster than it
was a moment ago.

You have good news and bad news.

The good news is that you have improved calculations for how to stop the
spinlock. They indicate that you actually need to identify the value after
0 in the current state of the circular buffer.

The bad news is that while you were determining this, the spinlock has just
finished inserting its fifty millionth value (50000000).

What is the value after 0 the moment 50000000 is inserted?
*/

require __DIR__ . '/../../../vendor/autoload.php';

/**
 * Determines what is the value after 0 the moment 50000000 is inserted.
 *
 * @param int $steps
 *
 * @return int
 */
function solution(int $steps): int
{
    $currentPosition = 0;

    for ($insert = 1; $insert <= 50000000; $insert++) {
        $currentPosition = ($currentPosition + $steps) % $insert;

        if ($currentPosition === 0) {
            $valueAfter0 = $insert;
        }

        $currentPosition++;
    }

    return $valueAfter0;
}

$steps = 345;

$valueAfter0 = solution($steps);
echo $valueAfter0;
