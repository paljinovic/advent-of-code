<?php

/*
--- Day 3: Spiral Memory ---

You come across an experimental new kind of memory stored on an infinite
two-dimensional grid.

Each square on the grid is allocated in a spiral pattern starting at a
location marked 1 and then counting up while spiraling outward. For
example, the first few squares are allocated like this:

17  16  15  14  13
18   5   4   3  12
19   6   1   2  11
20   7   8   9  10
21  22  23---> ...

While this is very space-efficient (no squares are skipped), requested data
must be carried back to square 1 (the location of the only access port for
this memory system) by programs that can only move up, down, left, or
right. They always take the shortest path: the Manhattan Distance between
the location of the data and square 1.

For example:

- Data from square 1 is carried 0 steps, since it's at the access port.
- Data from square 12 is carried 3 steps, such as: down, left, left.
- Data from square 23 is carried only 2 steps: up twice.
- Data from square 1024 must be carried 31 steps.

How many steps are required to carry the data from the square identified in
your puzzle input all the way to the access port?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Determines many steps are required to carry the data from the square identified in
 * your puzzle input all the way to the access port.
 *
 * @param int $square
 *
 * @return int Number of steps
 */
function solution(int $square): int
{
    $steps = 0;

    // In the right corner there is always odd number square
    $rightCornerSquare = getRightCornerSquareForSquareSpiral($square);
    $stepsInDirections = getStepsInEveryDirection($square, $rightCornerSquare);
    $halfStepsPerSide = (sqrt($rightCornerSquare) + 2 - 1) / 2;

    if ($stepsInDirections['right']) {
        $steps += abs($stepsInDirections['right'] - $halfStepsPerSide) + $halfStepsPerSide;
    } elseif ($stepsInDirections['down']) {
        $steps += abs($stepsInDirections['down'] - $halfStepsPerSide) + $halfStepsPerSide;
    } elseif ($stepsInDirections['left']) {
        $steps += abs($stepsInDirections['left'] - $halfStepsPerSide) + $halfStepsPerSide;
    } else {
        $steps += abs($stepsInDirections['up'] - $halfStepsPerSide) + $halfStepsPerSide;
    }

    return $steps;
}

function getRightCornerSquareForSquareSpiral($square): int
{
    $i = 1;
    $rightCornerSquare = 1;
    while ($i * $i <= $square) {
        if ($i * $i % 2 !== 0) {
            $rightCornerSquare = $i * $i;
        }

        $i++;
    }

    return $rightCornerSquare;
}

function getStepsInEveryDirection(int $square, int $rightCornerSquare): array
{
    $stepsInDirections = [
        'up' => 0,
        'left' => 0,
        'down' => 0,
        'right' => 0,
    ];

    $nextOddRightCornerSquareSqrt = sqrt($rightCornerSquare) + 2;
    $possibleStepsInSomeDirection = $nextOddRightCornerSquareSqrt - 1;

    foreach ($stepsInDirections as $direction => $directionSteps) {
        for ($i = 0; $i < $possibleStepsInSomeDirection; $i++) {
            if ($rightCornerSquare === $square) {
                break 2;
            }

            $stepsInDirections[$direction]++;
            $rightCornerSquare++;
        }
    }

    return $stepsInDirections;
}

$square = 361527;

$steps = solution($square);
echo $steps;
