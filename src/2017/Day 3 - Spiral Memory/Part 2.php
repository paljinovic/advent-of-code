<?php

/*
--- Part Two ---

As a stress test on the system, the programs here clear the grid and then
store the value 1 in square 1. Then, in the same allocation order as shown
above, they store the sum of the values in all adjacent squares, including
diagonals.

So, the first few squares' values are chosen as follows:

- Square 1 starts with the value 1.
- Square 2 has only one adjacent filled square (with value 1), so it
  also stores 1.
- Square 3 has both of the above squares as neighbors and stores the sum
  of their values, 2.
- Square 4 has all three of the aforementioned squares as neighbors and
  stores the sum of their values, 4.
- Square 5 only has the first and fourth squares as neighbors, so it
  gets the value 5.

Once a square is written, its value does not change. Therefore, the first
few squares would receive the following values:

147  142  133  122   59
304    5    4    2   57
330   10    1    1   54
351   11   23   25   26
362  747  806--->   ...

What is the first value written that is larger than your puzzle input?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Determines what is the first value written
 * that is larger than your puzzle input.
 *
 * @param int $squareInput
 *
 * @return int
 */
function solution(int $squareInput): int
{
    // Middle number 1 coordinates
    $squares[0][0] = 1;
    // Spiral number, first spiral is around number 1 [0, 0]
    $spiral = 1;

    // Horizontal and vertical position
    $horizontal = 0;
    $vertical = 0;

    $firstLargerThanInput = null;
    // While first larger than input is not found
    while (!isset($firstLargerThanInput)) {
        // Moving to the right
        $horizontal++;

        $squares[$horizontal][$vertical] = calculateSquareValue($squares, $horizontal, $vertical);
        if ($squares[$horizontal][$vertical] > $squareInput) {
            $firstLargerThanInput = $squares[$horizontal][$vertical];
            break;
        }

        $stepsInDirections = getStepsInDirectionForSpiral($spiral);

        // Calculating square value on every step
        foreach ($stepsInDirections as $direction => $steps) {
            for ($i = 0; $i < $steps; $i++) {
                switch ($direction) {
                    case 'up':
                        $vertical++;
                        break;
                    case 'left':
                        $horizontal--;
                        break;
                    case 'down':
                        $vertical--;
                        break;
                    case 'right':
                        $horizontal++;
                        break;
                    }

                $squares[$horizontal][$vertical] = calculateSquareValue($squares, $horizontal, $vertical);
                if ($squares[$horizontal][$vertical] > $squareInput) {
                    $firstLargerThanInput = $squares[$horizontal][$vertical];
                    break 3;
                }
            }
        }

        $spiral++;
    }

    return $firstLargerThanInput;
}

function getStepsInDirectionForSpiral(int $spiral): array
{
    $steps = 2 * $spiral;

    $stepsInDirections = [
        'up' => $steps - 1,
        'left' => $steps,
        'down' => $steps,
        'right' => $steps,
    ];

    return $stepsInDirections;
}

function calculateSquareValue(array $squares, int $horizontal, int $vertical): int
{
    $squareValue =
        + (isset($squares[$horizontal - 1][$vertical]) ? $squares[$horizontal - 1][$vertical] : 0)
        + (isset($squares[$horizontal][$vertical - 1]) ? $squares[$horizontal][$vertical - 1] : 0)
        + (isset($squares[$horizontal - 1][$vertical - 1]) ? $squares[$horizontal - 1][$vertical - 1] : 0)
        + (isset($squares[$horizontal + 1][$vertical - 1]) ? $squares[$horizontal + 1][$vertical - 1] : 0)
        + (isset($squares[$horizontal + 1][$vertical + 1]) ? $squares[$horizontal + 1][$vertical + 1] : 0)
        + (isset($squares[$horizontal + 1][$vertical]) ? $squares[$horizontal + 1][$vertical] : 0)
        + (isset($squares[$horizontal - 1][$vertical + 1]) ? $squares[$horizontal - 1][$vertical + 1] : 0)
        + (isset($squares[$horizontal][$vertical + 1]) ? $squares[$horizontal][$vertical + 1] : 0)
    ;

    return $squareValue;
}

$square = 361527;

$steps = solution($square);
echo $steps;
