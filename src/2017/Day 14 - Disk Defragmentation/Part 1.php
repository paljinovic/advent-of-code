<?php

/*
--- Day 14: Disk Defragmentation ---

Suddenly, a scheduled job activates the system's disk defragmenter. Were
the situation different, you might sit and watch it for a while, but today,
you just don't have that kind of time. It's soaking up valuable system
resources that are needed elsewhere, and so the only option is to help it
finish its task as soon as possible.

The disk in question consists of a 128x128 grid; each square of the grid is
either free or used. On this disk, the state of the grid is tracked by the
bits in a sequence of knot hashes.

A total of 128 knot hashes are calculated, each corresponding to a single
row in the grid; each hash contains 128 bits which correspond to individual
grid squares. Each bit of a hash indicates whether that square is free (0)
or used (1).

The hash inputs are a key string (your puzzle input), a dash, and a number
from 0 to 127 corresponding to the row. For example, if your key string
were flqrgnkx, then the first row would be given by the bits of the knot
hash of flqrgnkx-0, the second row from the bits of the knot hash of
flqrgnkx-1, and so on until the last row, flqrgnkx-127.

The output of a knot hash is traditionally represented by 32 hexadecimal
digits; each of these digits correspond to 4 bits, for a total of
4 * 32 = 128 bits. To convert to bits, turn each hexadecimal digit to its
equivalent binary value, high-bit first: 0 becomes 0000, 1 becomes 0001, e
becomes 1110, f becomes 1111, and so on; a hash that begins with a0c2017...
in hexadecimal would begin with 10100000110000100000000101110000... in
binary.

Continuing this process, the first 8 rows and columns for key flqrgnkx
appear as follows, using # to denote used squares, and . to denote free
ones:

##.#.#..-->
.#.#.#.#
....#.#.
#.#.##.#
.##.#...
##..#..#
.#...#..
##.#.##.-->
|      |
V      V
In this example, 8108 squares are used across the entire 128x128 grid.

Given your actual key string, how many squares are used?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Determines how many squares are used for key string.
 *
 * @param string $keyString
 *
 * @return int
 */
function solution(string $keyString): int
{
    $suffix = [17, 31, 73, 47, 23];
    $n = 256;

    $hexs = [];
    for ($row = 0; $row < 128; $row++) {
        $circularList = range(0, $n - 1);
        $currentPosition = 0;
        $skipSize = 0;

        $lengths = getLengths($row, $keyString, $suffix);

        for ($round = 0; $round < 64; $round++) {
            foreach ($lengths as $length) {
                $circularList = reverse($circularList, $length, $currentPosition);

                if (($length + $skipSize) % $n != 0) {
                    $currentPosition = ($currentPosition + $length + $skipSize) % $n;
                }

                $skipSize += 1;
                if ($skipSize === $n) {
                    $skipSize = 0;
                }
            }
        }

        $denseHash = [];
        for ($offset = 0; $offset < 256; $offset += 16) {
            $sixteenth = array_slice($circularList, $offset, 16);
            $denseHash[] = $sixteenth[0] ^ $sixteenth[1] ^ $sixteenth[2] ^ $sixteenth[3]
                ^ $sixteenth[4] ^ $sixteenth[5] ^ $sixteenth[6] ^ $sixteenth[7]
                ^ $sixteenth[8] ^ $sixteenth[9] ^ $sixteenth[10] ^ $sixteenth[11]
                ^ $sixteenth[12] ^ $sixteenth[13] ^ $sixteenth[14] ^ $sixteenth[15];
        }

        $hex = '';
        foreach ($denseHash as $integer) {
            $hex .= sprintf('%02x', $integer);
        }

        $hexs[] = $hex;
    }

    $usedSquares = countUsedSquares($hexs);

    return $usedSquares;
}

function getLengths(int $row, string $keyString, array $suffix): array
{
    $rowKeystring = $keyString.'-'.$row;
    for ($i = 0; $i < strlen($rowKeystring); $i++) {
        $lengths[] = ord($rowKeystring[$i]);
    }

    $lengths = array_merge($lengths, $suffix);

    return $lengths;
}

function reverse(array $circularList, int $length, int $currentPosition): array
{
    $n = count($circularList);

    $from = $currentPosition;
    $to = ($currentPosition + $length) % $n;

    $reverse = [];

    if ($from > $to) {
        for ($i = $from; $i < $n; $i++) {
            $reverse[$i] = $circularList[$i];
        }
        for ($i = 0; $i < $to; $i++) {
            $reverse[$i] = $circularList[$i];
        }
    } else {
        for ($i = $from; $i < $to; $i++) {
            $reverse[$i] = $circularList[$i];
        }
    }

    $keys = array_keys($reverse);
    $values = array_values($reverse);

    foreach ($keys as $key) {
        $endValue = end($values);
        $endKey = key($values);
        unset($values[$endKey]);

        $reverse[$key] = $endValue;
    }

    foreach ($circularList as $key => $value) {
        if (isset($reverse[$key])) {
            // If key is reversed

            $reversed[$key] = $reverse[$key];
        } else {
            // Take from circular list

            $reversed[$key] = $value;
        }
    }

    return $reversed;
}

function countUsedSquares(array $hexs): string
{
    $usedSquares = 0;

    foreach ($hexs as $hex) {
        $binary = '';
        foreach (str_split($hex) as $hexChar) {
            $binary .= str_pad(base_convert($hexChar, 16, 2), 4, '0', STR_PAD_LEFT);
        }

        $usedSquares += substr_count($binary, '1');
    }

    return $usedSquares;
}

$keyString = 'hwlqcszp';

$usedSquares = solution($keyString);
echo $usedSquares;
