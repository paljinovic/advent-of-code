<?php

/*
--- Part Two ---

Now, all the defragmenter needs to know is the number of regions. A region
is a group of used squares that are all adjacent, not including diagonals.
Every used square is in exactly one region: lone used squares form their
own isolated regions, while several adjacent squares all count as a single
region.

In the example above, the following nine regions are visible, each marked
with a distinct digit:

11.2.3..-->
.1.2.3.4
....5.6.
7.8.55.9
.88.5...
88..5..8
.8...8..
88.8.88.-->
|      |
V      V

Of particular interest is the region marked 8; while it does not appear
contiguous in this small view, all of the squares marked 8 are connected
when considering the whole 128x128 grid. In total, in this example, 1242
regions are present.

How many regions are present given your key string?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Determines how many squares are used for key string.
 *
 * @param string $keyString
 *
 * @return int
 */
function solution(string $keyString): int
{
    $suffix = [17, 31, 73, 47, 23];
    $n = 256;

    $grid = [];

    for ($row = 0; $row < 128; $row++) {
        $circularList = range(0, $n - 1);
        $currentPosition = 0;
        $skipSize = 0;

        $lengths = getLengths($row, $keyString, $suffix);

        for ($round = 0; $round < 64; $round++) {
            foreach ($lengths as $length) {
                $circularList = reverse($circularList, $length, $currentPosition);

                if (($length + $skipSize) % $n != 0) {
                    $currentPosition = ($currentPosition + $length + $skipSize) % $n;
                }

                $skipSize += 1;
                if ($skipSize === $n) {
                    $skipSize = 0;
                }
            }
        }

        $denseHash = [];
        for ($offset = 0; $offset < 256; $offset += 16) {
            $sixteenth = array_slice($circularList, $offset, 16);
            $denseHash[] = $sixteenth[0] ^ $sixteenth[1] ^ $sixteenth[2] ^ $sixteenth[3]
                ^ $sixteenth[4] ^ $sixteenth[5] ^ $sixteenth[6] ^ $sixteenth[7]
                ^ $sixteenth[8] ^ $sixteenth[9] ^ $sixteenth[10] ^ $sixteenth[11]
                ^ $sixteenth[12] ^ $sixteenth[13] ^ $sixteenth[14] ^ $sixteenth[15];
        }

        $binary = '';
        foreach ($denseHash as $integer) {
            $binary .= sprintf('%08b', $integer);
        }

        for ($i = 0; $i < 128; $i++) {
            $grid[$row][$i] = $binary[$i] ? 1 : 0;
        }
    }

    $regions = countRegions($grid);

    return $regions;
}

function getLengths(int $row, string $keyString, array $suffix): array
{
    $rowKeystring = $keyString.'-'.$row;
    for ($i = 0; $i < strlen($rowKeystring); $i++) {
        $lengths[] = ord($rowKeystring[$i]);
    }

    $lengths = array_merge($lengths, $suffix);

    return $lengths;
}

function reverse(array $circularList, int $length, int $currentPosition): array
{
    $n = count($circularList);

    $from = $currentPosition;
    $to = ($currentPosition + $length) % $n;

    $reverse = [];

    if ($from > $to) {
        for ($i = $from; $i < $n; $i++) {
            $reverse[$i] = $circularList[$i];
        }
        for ($i = 0; $i < $to; $i++) {
            $reverse[$i] = $circularList[$i];
        }
    } else {
        for ($i = $from; $i < $to; $i++) {
            $reverse[$i] = $circularList[$i];
        }
    }

    $keys = array_keys($reverse);
    $values = array_values($reverse);

    foreach ($keys as $key) {
        $endValue = end($values);
        $endKey = key($values);
        unset($values[$endKey]);

        $reverse[$key] = $endValue;
    }

    foreach ($circularList as $key => $value) {
        if (isset($reverse[$key])) {
            // If key is reversed

            $reversed[$key] = $reverse[$key];
        } else {
            // Take from circular list

            $reversed[$key] = $value;
        }
    }

    return $reversed;
}

function countRegions(array $grid): string
{
    $regions = 0;

    for ($y = 0; $y < 128; $y++) {
        for ($x = 0; $x < 128; $x++) {
            if ($grid[$y][$x] === 1) {
                $regions++;

                $markNeighbours = [[$x, $y]];
                while (count($markNeighbours) > 0) {
                    [$markX, $markY] = array_shift($markNeighbours);

                    if ($markX >= 0 && $markX < 128 && $markY >= 0 && $markY < 128) {
                        if($grid[$markY][$markX] === 1) {
                            $grid[$markY][$markX] = 0;

                            $markNeighbours[] = [$markX + 1, $markY];
                            $markNeighbours[] = [$markX - 1, $markY];
                            $markNeighbours[] = [$markX, $markY + 1];
                            $markNeighbours[] = [$markX, $markY - 1];
                        }
                    }
                }
            }
        }
    }

    return $regions;
}

$keyString = 'hwlqcszp';

$regions = solution($keyString);
echo $regions;
