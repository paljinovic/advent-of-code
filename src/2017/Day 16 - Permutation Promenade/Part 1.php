<?php

/*
--- Day 16: Permutation Promenade ---

You come upon a very unusual sight; a group of programs here appear to be
dancing.

There are sixteen programs in total, named a through p. They start by
standing in a line: a stands in position 0, b stands in position 1, and so
on until p, which stands in position 15.

The programs' dance consists of a sequence of dance moves:

- Spin, written sX, makes X programs move from the end to the front, but
  maintain their order otherwise. (For example, s3 on abcde produces
  cdeab).
- Exchange, written xA/B, makes the programs at positions A and B swap
  places.
- Partner, written pA/B, makes the programs named A and B swap places.

For example, with only five programs standing in a line (abcde), they could
do the following dance:

- s1, a spin of size 1: eabcd.
- x3/4, swapping the last two programs: eabdc.
- pe/b, swapping programs e and b: baedc.

After finishing their dance, the programs end up in order baedc.

You watch the dance for a while and record their dance moves (your puzzle
input). In what order are the programs standing after their dance?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Determines programs order after their dance.
 *
 * @param string $programs
 * @param string $danceMoves
 *
 * @return string
 */
function solution(string $programs, string $danceMoves): string
{
    $danceMoves = explode(',', $danceMoves);

    foreach ($danceMoves as $danceMove) {
        if (preg_match('/s(\d+)/', $danceMove, $matches)) {
            $programs = spin($programs, $matches[1]);
        } elseif (preg_match('/x(\d+)\/(\d+)/', $danceMove, $matches)) {
            $programs = exchange($programs, $matches[1], $matches[2]);
        } elseif (preg_match('/p(\w)\/(\w)/', $danceMove, $matches)) {
            $programs = partner($programs, $matches[1], $matches[2]);
        }
    }

    return $programs;
}

function spin(string $programs, int $move): string
{
    $programs = substr($programs, -$move).substr($programs, 0, -$move);

    return $programs;
}

function exchange(string $programs, int $positionA, int $positionB): string
{
    $temp = $programs[$positionA];
    $programs[$positionA] = $programs[$positionB];
    $programs[$positionB] = $temp;

    return $programs;
}

function partner(string $programs, string $programA, string $programB): string
{
    $positionA = strpos($programs, $programA);
    $positionB = strpos($programs, $programB);

    $programs = exchange($programs, $positionA, $positionB);

    return $programs;
}

$programs = 'abcdefghijklmnop';
$danceMoves = file_get_contents('danceMoves.txt');

$programs = solution($programs, $danceMoves);
echo $programs;
