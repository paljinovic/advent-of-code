<?php

/*
--- Part Two ---

Now that you're starting to get a feel for the dance moves, you turn your
attention to the dance as a whole.

Keeping the positions they ended up in from their previous dance, the
programs perform it again and again: including the first dance, a total of
one billion (1000000000) times.

In the example above, their second dance would begin with the order baedc,
and use the same dance moves:

- s1, a spin of size 1: cbaed.
- x3/4, swapping the last two programs: cbade.
- pe/b, swapping programs e and b: ceadb.

In what order are the programs standing after their billion dances?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Determines in what order are the programs standing after their billion dances.
 *
 * @param string $programs
 * @param string $danceMoves
 *
 * @return string
 */
function solution(string $programs, string $danceMoves): string
{
    $startPrograms = $programs;
    $danceMoves = explode(',', $danceMoves);
    $totalMoves = 1000000000;

    $fullCycle = false;
    $danceMovesForFullCycle = 0;
    $programsOrderCache = [];
    $programsOrderCache[$programs] = 1;

    while (!$fullCycle) {
        $programs = dance($programs, $danceMoves);

        $danceMovesForFullCycle++;

        if (isset($programsOrderCache[$programs])) {
            $fullCycle = true;
        }

        $programsOrderCache[$programs] = 1;
    }

    $dances = $totalMoves % $danceMovesForFullCycle;
    for ($i = 0; $i < $dances; $i++) {
        $programs = dance($programs, $danceMoves);
    }

    return $programs;
}

function dance(string $programs, array $danceMoves): string
{
    foreach ($danceMoves as $danceMove) {
        if (preg_match('/s(\d+)/', $danceMove, $matches)) {
            $programs = spin($programs, $matches[1]);
        } elseif (preg_match('/x(\d+)\/(\d+)/', $danceMove, $matches)) {
            $programs = exchange($programs, $matches[1], $matches[2]);
        } elseif (preg_match('/p(\w)\/(\w)/', $danceMove, $matches)) {
            $programs = partner($programs, $matches[1], $matches[2]);
        }
    }

    return $programs;
}

function spin(string $programs, int $move): string
{
    $programs = substr($programs, -$move).substr($programs, 0, -$move);

    return $programs;
}

function exchange(string $programs, int $positionA, int $positionB): string
{
    $temp = $programs[$positionA];
    $programs[$positionA] = $programs[$positionB];
    $programs[$positionB] = $temp;

    return $programs;
}

function partner(string $programs, string $programA, string $programB): string
{
    $positionA = strpos($programs, $programA);
    $positionB = strpos($programs, $programB);

    $programs = exchange($programs, $positionA, $positionB);

    return $programs;
}

$programs = 'abcdefghijklmnop';
$danceMoves = file_get_contents('danceMoves.txt');

$programs = solution($programs, $danceMoves);
echo $programs;
