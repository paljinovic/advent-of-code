<?php

/*
--- Part Two ---

Now, you need to pass through the firewall without being caught - easier
said than done.

You can't control the speed of the packet, but you can delay it any number
of picoseconds. For each picosecond you delay the packet before beginning
your trip, all security scanners move one step. You're not in the firewall
during this time; you don't enter layer 0 until you stop delaying the
packet.

In the example above, if you delay 10 picoseconds (picoseconds 0 - 9), you
won't get caught:

State after delaying:
 0   1   2   3   4   5   6
[ ] [S] ... ... [ ] ... [ ]
[ ] [ ]         [ ]     [ ]
[S]             [S]     [S]
                [ ]     [ ]

Picosecond 10:
 0   1   2   3   4   5   6
( ) [S] ... ... [ ] ... [ ]
[ ] [ ]         [ ]     [ ]
[S]             [S]     [S]
                [ ]     [ ]

 0   1   2   3   4   5   6
( ) [ ] ... ... [ ] ... [ ]
[S] [S]         [S]     [S]
[ ]             [ ]     [ ]
                [ ]     [ ]


Picosecond 11:
 0   1   2   3   4   5   6
[ ] ( ) ... ... [ ] ... [ ]
[S] [S]         [S]     [S]
[ ]             [ ]     [ ]
                [ ]     [ ]

 0   1   2   3   4   5   6
[S] (S) ... ... [S] ... [S]
[ ] [ ]         [ ]     [ ]
[ ]             [ ]     [ ]
                [ ]     [ ]


Picosecond 12:
 0   1   2   3   4   5   6
[S] [S] (.) ... [S] ... [S]
[ ] [ ]         [ ]     [ ]
[ ]             [ ]     [ ]
                [ ]     [ ]

 0   1   2   3   4   5   6
[ ] [ ] (.) ... [ ] ... [ ]
[S] [S]         [S]     [S]
[ ]             [ ]     [ ]
                [ ]     [ ]


Picosecond 13:
 0   1   2   3   4   5   6
[ ] [ ] ... (.) [ ] ... [ ]
[S] [S]         [S]     [S]
[ ]             [ ]     [ ]
                [ ]     [ ]

 0   1   2   3   4   5   6
[ ] [S] ... (.) [ ] ... [ ]
[ ] [ ]         [ ]     [ ]
[S]             [S]     [S]
                [ ]     [ ]


Picosecond 14:
 0   1   2   3   4   5   6
[ ] [S] ... ... ( ) ... [ ]
[ ] [ ]         [ ]     [ ]
[S]             [S]     [S]
                [ ]     [ ]

 0   1   2   3   4   5   6
[ ] [ ] ... ... ( ) ... [ ]
[S] [S]         [ ]     [ ]
[ ]             [ ]     [ ]
                [S]     [S]


Picosecond 15:
 0   1   2   3   4   5   6
[ ] [ ] ... ... [ ] (.) [ ]
[S] [S]         [ ]     [ ]
[ ]             [ ]     [ ]
                [S]     [S]

 0   1   2   3   4   5   6
[S] [S] ... ... [ ] (.) [ ]
[ ] [ ]         [ ]     [ ]
[ ]             [S]     [S]
                [ ]     [ ]


Picosecond 16:
 0   1   2   3   4   5   6
[S] [S] ... ... [ ] ... ( )
[ ] [ ]         [ ]     [ ]
[ ]             [S]     [S]
                [ ]     [ ]

 0   1   2   3   4   5   6
[ ] [ ] ... ... [ ] ... ( )
[S] [S]         [S]     [S]
[ ]             [ ]     [ ]
                [ ]     [ ]

Because all smaller delays would get you caught, the fewest number of
picoseconds you would need to delay to get through safely is 10.

What is the fewest number of picoseconds that you need to delay the packet
to pass through the firewall without being caught?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Determines what is the fewest number of picoseconds that you need
 * to delay the packet to pass through the firewall without being caught.
 *
 * @param string $layerRangesString
 *
 * @return int
 */
function solution(string $layerRangesString): int
{
    set_time_limit(300);

    $delayPicoseconds = 0;

    $layerRanges = getLayerRanges($layerRangesString);
    $scanners = getScannersPositionsAndDirections($layerRanges);
    $scannersAt[0] = $scanners;
    $maxLayer = max(array_keys($scanners));

    $isCaught = true;
    while ($isCaught) {
        $scanners = $scannersAt[$delayPicoseconds];

        // Clean memory
        if ($delayPicoseconds !== 0) {
            unset($scannersAt[$delayPicoseconds]);
        }

        $isCaught = false;
        $step = 0;
        while ($step <= $maxLayer) {
            foreach ($scanners as $layer => $scanner) {
                if ($step === $layer && $scanner['ranges'][0] === 1) {
                    $isCaught = true;
                    // If next step is calculated
                    if (isset($scannersAt[$delayPicoseconds + $step + 1])) {
                        break 2;
                    }
                }

                $scannerOn = array_search(1, $scanner['ranges']);
                $scanners[$layer]['ranges'][$scannerOn] = 0;

                if ($scanner['direction'] === 'down') {
                    $scanners[$layer]['ranges'][$scannerOn + 1] = 1;
                    if ($scannerOn + 1 === count($scanner['ranges']) - 1) {
                        $scanners[$layer]['direction'] = 'up';
                    }
                } else {
                    $scanners[$layer]['ranges'][$scannerOn - 1] = 1;
                    if ($scannerOn - 1 === 0) {
                        $scanners[$layer]['direction'] = 'down';
                    }
                }
            }

            $step += 1;

            if (!isset($scannersAt[$delayPicoseconds + $step])) {
                $scannersAt[$delayPicoseconds + $step] = $scanners;
            }

            if ($isCaught) {
                break;
            }
        }

        if ($isCaught) {
            $delayPicoseconds++;
        }
    }

    return $delayPicoseconds;
}

function getLayerRanges(string $layerRangesString): array
{
    $layerRanges = [];

    $layerRangesArray = array_map('trim', explode("\n", trim($layerRangesString)));
    foreach ($layerRangesArray as $value) {
        list($layer, $range) = explode(': ', $value);
        $layerRanges[(int) $layer] = (int) $range;
    }

    return $layerRanges;
}

function getScannersPositionsAndDirections(array $layerRanges): array
{
    $scanner = [];
    foreach ($layerRanges as $layer => $range) {
        $ranges = [0 => 1] + array_fill(1, $range - 1, 0);
        $scanner[$layer]['direction'] = 'down';
        $scanner[$layer]['ranges'] = $ranges;
    }

    return $scanner;
}

function delayPicoseconds(array $scanners, $delayPicoseconds): array
{
    $step = 0;
    while ($step < $delayPicoseconds) {
        foreach ($scanners as $layer => $scanner) {
            $scannerOn = array_search(1, $scanner['ranges']);
            $scanners[$layer]['ranges'][$scannerOn] = 0;

            if ($scanner['direction'] === 'down') {
                $scanners[$layer]['ranges'][$scannerOn + 1] = 1;
                if ($scannerOn + 1 === count($scanner['ranges']) - 1) {
                    $scanners[$layer]['direction'] = 'up';
                }
            } else {
                $scanners[$layer]['ranges'][$scannerOn - 1] = 1;
                if ($scannerOn - 1 === 0) {
                    $scanners[$layer]['direction'] = 'down';
                }
            }
        }

        $step += 1;
    }

    return $scanners;
}

$layerRanges = '0: 5
    1: 2
    2: 3
    4: 4
    6: 6
    8: 4
    10: 8
    12: 6
    14: 6
    16: 14
    18: 6
    20: 8
    22: 8
    24: 10
    26: 8
    28: 8
    30: 10
    32: 8
    34: 12
    36: 9
    38: 20
    40: 12
    42: 12
    44: 12
    46: 12
    48: 12
    50: 12
    52: 12
    54: 12
    56: 14
    58: 14
    60: 14
    62: 20
    64: 14
    66: 14
    70: 14
    72: 14
    74: 14
    76: 14
    78: 14
    80: 12
    90: 30
    92: 17
    94: 18';

$delayPicoseconds = solution($layerRanges);
echo $delayPicoseconds;
