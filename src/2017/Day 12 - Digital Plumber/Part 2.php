<?php

/*
--- Part Two ---

There are more programs than just the ones in the group containing program
ID 0. The rest of them have no way of reaching that group, and still might
have no way of reaching each other.

A group is a collection of programs that can all communicate via pipes
either directly or indirectly. The programs you identified just a moment
ago are all part of the same group. Now, they would like you to determine
the total number of groups.

In the example above, there were 2 groups: one consisting of programs
0,2,3,4,5,6, and the other consisting solely of program 1.

How many groups are there in total?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Determines how many groups are there in total.
 *
 * @param string $communicationListString
 *
 * @return int
 */
function solution(string $communicationListString): int
{
    $groups = [];
    $inGroup = [];

    $communicationList = getCommunicationList($communicationListString);
        foreach (array_keys($communicationList) as $key => $program) {
            // If program already belongs to some group
            if (in_array($program, $inGroup)) {
                continue;
            }

            // If program is not in any group

            $groups[$key] = getConnectedPrograms($communicationList, $program, []);
            sort($groups[$key]);

            $inGroup = array_unique(array_merge($inGroup, $groups[$key]));
    }

    return count($groups);
}

function getCommunicationList(string $communicationListString): array
{
    $communicationListArray = array_map('trim', explode("\n", trim($communicationListString)));
    $communicationList = [];

    foreach ($communicationListArray as $value) {
        list($program, $communicatesWith) = explode(' <-> ', $value);
        $communicationList[(int) $program] = array_map('intval', explode(',', $communicatesWith));
    }

    return $communicationList;
}

function getConnectedPrograms(array $communicationList, int $program, array $connectedPrograms): array
{
    $newConnectedPrograms = array_diff($communicationList[$program], $connectedPrograms);
    $connectedPrograms = array_merge($connectedPrograms, $newConnectedPrograms);

    foreach ($newConnectedPrograms as $connectedProgram) {
        $connectedPrograms = array_unique(array_merge(
            $connectedPrograms,
            getConnectedPrograms($communicationList, $connectedProgram, $connectedPrograms)
        ));
    }

    return $connectedPrograms;
}

$communicationListString = file_get_contents('communicationList.txt');

$groups = solution($communicationListString);
echo $groups;
