<?php

/*
--- Part Two ---

How many steps away is the furthest he ever got from his starting position?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Determines the furthest number of steps child moved from starting position.
 *
 * @param string $path
 *
 * @return int
 */
function solution(string $path): int
{
    // Axial coordinates: https://www.redblobgames.com/grids/hexagons/
    // http://3dmdesign.com/development/hexmap-coordinates-the-easy-way
    $x = 0;
    $y = 0;

    $pathArray = array_map('trim', explode(',', $path));

    $max = 0;

    foreach ($pathArray as $direction) {
        switch ($direction)
        {
            case 'n':
                $y++;
                break;
            case 'nw':
                $x--;
                break;
            case 'ne':
                $x++;
                $y++;
                break;
            case 's':
                $y--;
                break;
            case 'sw':
                $x--;
                $y--;
                break;
            case 'se':
                $x++;
                break;
        }

        $steps = max(abs($y), max(abs($x), abs(($x - $y) * -1)));
        if ($steps > $max) {
            $max = $steps;
        }
    }

    return $max;
}

$path = file_get_contents('path.txt');

$steps = solution($path);
echo $steps;
