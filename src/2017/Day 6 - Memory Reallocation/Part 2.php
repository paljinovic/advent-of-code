<?php

/*
--- Part Two ---

Out of curiosity, the debugger would also like to know the size of the
loop: starting from a state that has already been seen, how many block
redistribution cycles must be performed before that same state is seen
again?

In the example above, 2 4 1 2 is seen again after four cycles, and so the
answer in that example would be 4.

How many cycles are in the infinite loop that arises from the configuration
in your puzzle input?
*/

require __DIR__.'/../../../vendor/autoload.php';

/**
 * Determines how many redistribution cycles must be completed
 * before a configuration is produced that has been seen before.
 *
 * @param array $banksBlocks
 *
 * @return int Redistribution cycles
 */
function solution(array $banksBlocks): int
{
    $searchedBlockRedistributionCycles = 0;
    $searchedBlockString = findRepeatingBlock($banksBlocks);
    $banksBlocks = explode(',', $searchedBlockString);

    $banksCount = count($banksBlocks);
    $searchedBlockCycles = [];

    while (!in_array($searchedBlockString, $searchedBlockCycles)) {
        $max = max($banksBlocks);
        $startingBank = array_search($max, $banksBlocks);
        $redistribute = (int) ceil($max / $banksCount);

        $currentBank = null;
        while ($currentBank !== $startingBank) {
            if (!isset($currentBank)) {
                $currentBank = $startingBank;
            }

            if ($currentBank === $banksCount - 1) {
                $currentBank = 0;
            } else {
                $currentBank += 1;
            }

            $banksBlocks[$currentBank] += $redistribute;
            $banksBlocks[$startingBank] -= $redistribute;

            $max = $max - $redistribute;
            if ($max < $redistribute) {
                $redistribute = $max;
            }
        }

        $searchedBlockCycles[] = implode(',', $banksBlocks);

        $searchedBlockRedistributionCycles++;
    }

    return $searchedBlockRedistributionCycles;
}

function findRepeatingBlock(array $banksBlocks): string
{
    $banksCount = count($banksBlocks);
    $redistributedBankBlocksStrings = [];

    $repeated = false;
    while (!$repeated) {
        $max = max($banksBlocks);
        $startingBank = array_search($max, $banksBlocks);
        $redistribute = (int) ceil($max / $banksCount);

        $currentBank = null;
        while ($currentBank !== $startingBank) {
            if (!isset($currentBank)) {
                $currentBank = $startingBank;
            }

            if ($currentBank === $banksCount - 1) {
                $currentBank = 0;
            } else {
                $currentBank += 1;
            }

            $banksBlocks[$currentBank] += $redistribute;
            $banksBlocks[$startingBank] -= $redistribute;

            $max = $max - $redistribute;
            if ($max < $redistribute) {
                $redistribute = $max;
            }
        }

        $bankBlocksString = implode(',', $banksBlocks);
        if (in_array($bankBlocksString, $redistributedBankBlocksStrings)) {
            $repeated = true;
        } else {
            $redistributedBankBlocksStrings[] = implode(',', $banksBlocks);
        }
    }

    return $bankBlocksString;
}

$banksBlocks = [
    0,
    5,
    10,
    0,
    11,
    14,
    13,
    4,
    11,
    8,
    8,
    7,
    1,
    4,
    12,
    11
];

$redistributionCycles = solution($banksBlocks);
echo $redistributionCycles;
